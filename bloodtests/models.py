from django.db import models


class Test(models.Model):
    code = models.CharField(max_length=4)
    name = models.CharField(max_length=100)
    unit = models.CharField(max_length=10)
    lower = models.FloatField(null=True, blank=True)
    upper = models.FloatField(null=True, blank=True)
