from rest_framework import serializers

from bloodtests.models import Test


class TestInputSerializer(serializers.ModelSerializer):
    def validate(self, data):
        if data.get('lower') is None and data.get('upper') is None:
            # it is uncommon format for such errors
            raise serializers.ValidationError(
                {'Lower and upper cannot both be null': "Lower and upper cannot both be null"})
        if data.get('lower') and data.get('upper') and data.get('lower') > data.get('upper'):
            raise serializers.ValidationError(
                {"Lower value can't exceed upper value": "Lower value can't exceed upper value"})
        return data

    class Meta:
        model = Test
        fields = ['code', 'lower', 'name', 'unit', 'upper']
        validators = []


class TestSerializer(serializers.ModelSerializer):
    ideal_range = serializers.SerializerMethodField()

    class Meta:
        model = Test
        fields = ['code', 'lower', 'name', 'unit', 'upper', 'ideal_range']

    def get_ideal_range(self, obj: Test) -> str:
        if obj.lower is None and obj.upper is None: return ''
        if obj.lower is None: return f'value <= {float(obj.upper)}'
        if obj.upper is None: return f'value >= {float(obj.lower)}'
        return f'{float(obj.lower)} <= value <= {float(obj.upper)}'
