from django.shortcuts import get_object_or_404
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from bloodtests.models import Test
from bloodtests.serializers import TestSerializer, TestInputSerializer


class TestDetails(APIView):
    def get(self, request: Request, code: str) -> Response:
        object = get_object_or_404(Test, code=code)
        return Response(TestSerializer(instance=object).data)

    def post(self, request: Request, code: str) -> Response:
        serializer = TestInputSerializer(data=request.data)
        if serializer.is_valid():
            object, _ = Test.objects.update_or_create(code=code,
                                                      defaults={
                                                          'name': serializer.validated_data.get('name'),
                                                          'unit': serializer.validated_data.get('unit'),
                                                          'lower': serializer.validated_data.get('lower'),
                                                          'upper': serializer.validated_data.get('upper'),
                                                      }, )
            output_serializer = TestSerializer(object)
            return Response(output_serializer.data)
        else:
            return Response(serializer.errors, 400)
